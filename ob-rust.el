;;; ob-rust.el --- org-babel support for rust

;; Copyright (C) Andrew Yoon

;; Author: Andrew Yoon
;; Keywords: literate programming, reproducible research, rust

;;; License:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Requirements:

;; Cargo
;; Rustc

;;; Commentary
;; This file enables compilation and execution of rust programs in org babel
;; through the `org-babel-execute-src-block' function.

;;; Code:
(require 'ob)
(require 'rust-mode)

(add-to-list 'org-babel-tangle-lang-exts '("rust" . "rs"))

(defvar org-babel-default-header-args:rust '())

(defun ob-rust--var-to-template (var)
  (format "%S" var))

(defun ob-rust--unpack-crates (processed-params)
  (let ((crates (cdr (assq :crates processed-params))))
    (if crates (mapcar 'string-trim (split-string crates ","))
      nil)
    )
)

(defun ob-rust--crate-to-extern-declaration (crate-slug)
  (concat "#[macro_use] extern crate "
          (car (split-string (format "%s" crate-slug) "=")) ";"))

(defun ob-rust--build-cargo-toml (rust-crate-slug crates)
  (concat "[package]\n"
          "name = \"" rust-crate-slug "\"\n"
          "version = \"0.0.1\"\n"
          "\n"
          "[dependencies]\n"
          (mapconcat 'identity crates "\n")
          "\n"))

(defun ob-rust--build-cargo-run-command (rust-crate-cargo-toml-path)
  (format "cargo run --manifest-path %s" rust-crate-cargo-toml-path))

(defun org-babel-expand-body:rust (body processed-params)
  "Expand BODY according to PARAMS, return the expanded body."
  (let* ((use-params (or processed-params (org-babel-process-params params)))
         (crates (ob-rust--unpack-crates processed-params)))
    (concat
     (mapconcat 'ob-rust--crate-to-extern-declaration crates "\n")
     "\n" body "\n")))


(defun org-babel-execute:rust (body params)
  "Compile and execute a block of Rust code with org-babel.
This function is called by `org-babel-execute-src-block'"
  (let* ((processed-params (org-babel-process-params params))
         (vars (nth 2 processed-params))
         (result-params (nth 3 processed-params))
         (result-type (nth 4 processed-params))
         (crates (ob-rust--unpack-crates processed-params))
         (full-body (org-babel-expand-body:rust body processed-params))
         (rust-crate-slug (substring (md5 (mapconcat 'identity crates "+")) 0 10))
         (rust-crate-dir (org-babel-process-file-name
                          (concat org-babel-temporary-directory "/" rust-crate-slug)))
         (rust-crate-src-dir (org-babel-process-file-name
                              (concat rust-crate-dir "/src")))
         (rust-crate-cargo-toml-path (org-babel-process-file-name
                                      (concat rust-crate-dir "/Cargo.toml")))
         (rust-crate-bin-src-name (org-babel-process-file-name
                                   (concat rust-crate-src-dir "/main.rs")))
         )
    (make-directory rust-crate-src-dir t)
    (delete-file rust-crate-cargo-toml-path)
    (delete-file rust-crate-bin-src-name)
    (write-region
     (ob-rust--build-cargo-toml rust-crate-slug crates)
     nil rust-crate-cargo-toml-path)
    (write-region full-body nil rust-crate-bin-src-name)
    (let ((results (org-babel-eval
                    (ob-rust--build-cargo-run-command rust-crate-cargo-toml-path)
                    "")))
      (when results
	      (setq results (org-trim (org-remove-indentation results)))
	      (org-babel-reassemble-table
	       (org-babel-result-cond (cdr (assq :result-params params))
	         (org-babel-read results t)
	         (let ((tmp-file (org-babel-temp-file "rust-")))
	           (with-temp-file tmp-file (insert results))
	           (org-babel-import-elisp-from-file tmp-file)))
         nil
         nil
	       )
        )
      )
    ))

(provide 'ob-rust)
;;; ob-rust.el ends here
